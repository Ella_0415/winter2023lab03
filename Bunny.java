public class Bunny
{
	public String name;
	public String favoriteFood;
	public boolean behaviour;
	
	public void bunnyJump(String bunnyName)
	{
		System.out.println(bunnyName + " has successfully jumped");
	}
	
	public void feedBunny(boolean bunnyBehaviour, String bunnyFood)
	{
		System.out.println("is bunny nice? ");
		if(bunnyBehaviour = false)
		{
			System.out.println("Bad bunny! You only get some hay!");
		}
		else
		{
			System.out.println("Good bunny! You can have some " + bunnyFood);
		}
	}
}