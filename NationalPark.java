import java.util.Scanner;
public class NationalPark
{
	public static void main (String[] args)
	{
		Scanner scan = new Scanner(System.in);
		Bunny[] fluffle = new Bunny[4];
		
		for(int i = 0; i < fluffle.length; i++)
		{
			fluffle[i] = new Bunny();
			System.out.println("What is your bunny's name?");
			fluffle[i].name = scan.next();
			System.out.println("What is your bunny's favorite food?");
			fluffle[i].favoriteFood = scan.next();
			System.out.println("true or false your bunny is nice");
			fluffle[i].behaviour = scan.nextBoolean();
		}
		System.out.println("The last bunny's information: ");
		System.out.println("Name: " + fluffle[3].name);
		System.out.println("Favorite food: " + fluffle[3].favoriteFood);
		System.out.println("It's a nice bunny: " + fluffle[3].behaviour);
		
		Bunny bun = new Bunny();
		System.out.println("The first bunny has something to show us!");
		bun.bunnyJump(fluffle[0].name);
		System.out.println("Let's see if the bunny has been nice!");
		bun.feedBunny(fluffle[0].behaviour, fluffle[0].favoriteFood);
		
	}
}